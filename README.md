# Controle de usuário

O código nesse repositório é um exemplo de como fazer um controle de usuário com senhas guardadas em forma de hash para 7 usuários com 3 tipos de nível (ou hierarquia) diferente.

O código lê as entradas que damos de senha e login apos isso ele pega essa senha transforma num hash e compara com um salvo dentro do código, apos isso ele compara logins e senhas e se for igual ao do sistema ele manda uma mensagem dizendo que o acesso foi possivel caso o contrario ele fala que voce não passará.

Cada usuário tem senhas e classes diferentes
