import hashlib


# Antes de tudo o ideal é fazer uma função que transforme a senha de cada usuário em hash para maior segurança:

def senha_hash(senha):
    Hash = hashlib.sha256()

    # As streams de senha seram passadas para UTF-8 antes de se tornarem hash para ter-se a certeza de que é possivel
    # colocar caracteres especiais para a senha (como @, á, õ, etc...)
    Hash.update(senha.encode('utf-8'))

    # senha Hash em hexadecimal ( a pesar de poder outros formatos)
    senha_hash = Hash.hexdigest()

    return senha_hash


class Usuario:
    def __init__(self, user, senha, classe):
        self.user = user
        self.senha = senha_hash(senha)
        self.classe = classe

#lista de usuários, suas senhas como hash e suas classes
usuario1 = Usuario("zeroberto", "@ fuciona?", "admin")
usuario2 = Usuario("berozerto", "hashsucks", "moderador")
usuario3 = Usuario("hipopotamo", "magoimplacavel", "usuario")
usuario4 = Usuario("jovemmeliante", "adorosenhas", "usuario")
usuario5 = Usuario("amadordelolis", "vidainfernal", "usuario")
usuario6 = Usuario("peixeboi", "canlhas_do_caaso", "moderador")


usuarios = [usuario1, usuario2, usuario3, usuario4, usuario5, usuario6]

#Função que verifica se usuário bate com senha caso positivo retorna true em caso negativo false
def verifica_usuario(username, senha):
    for usuario in usuarios:
        if usuario.user == username and usuario.senha == senha_hash(senha):
            return usuario
    return None



#Entrada para dar login
usuario_digitado = input("Digite seu Login: ")
senha_digitada = input("Digite a sua senha: ")

usuario_autenticado = verifica_usuario(usuario_digitado, senha_digitada)

#Checa se a senha e o login é igual ao dados do usuario e a senha em hash do sistema e dai printa se tem acesso ou não
if verifica_usuario(usuario_digitado, senha_digitada):
    print("Acertou Miseravi\n")
    print(f"Classe do usuário: {usuario_autenticado.classe}")

else:
    print("YOU SHALL NOT PASS!!!")